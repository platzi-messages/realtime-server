# Backend Exercise


### Installing

To install the project in your server/machine you need tu build the docker-containers with the command:

```
docker-compose build
```

after you can start the services with the command:

```
docker-compose up
```

open the api endpont in your navigator.

```
http://<your ip>:port/api/
```



## Built With
* [docker](https://docs.docker.com/) - Virtualization technology
* [redis](https://redis.io/documentation) - Open source, in-memory data structure store, used as a database
* [node](https://nodejs.org/es/docs/) - JavaScript runtime

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Author

* **Luis Antonio Rodriguez Garcia** - [luisrdz5](https://github.com/luisrdz5)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details










