
const formatMessage = require('./utils/messages');
const celery = require('celery-node');

const redisServer = process.env.REDIS_SERVER || '127.0.0.1';
const brokerUrl = `redis://${redisServer}:6379/0`;
const backend = `redis://${redisServer}`;

const botName = 'Platzi-Message Bot';

const {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers
} = require('./utils/users');

const client = celery.createClient(brokerUrl, backend);

function getSocketRoutes(io) {
  io.on('connection', socket => {
    console.log('Client connected');
    socket.emit('hello', { hello: 'backend' })

    socket.on('joinRoom', ({ username, room }) => {
      const user = userJoin(socket.id, username, room);

      socket.join(user.room);

      // Welcome current user
      socket.emit('message', formatMessage(botName, 'Welcome to Platzi Message!'));

      // Broadcast when a user connects
      socket.broadcast
        .to(user.room)
        .emit(
          'message',
          formatMessage(botName, `${user.username} has joined the chat`)
        );

      // Send users and room info
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      });
    });

    // Listen for Message
    socket.on('chatMessage', msg => {
      const user = getCurrentUser(socket.id);
      //const task = client.createTask('tasks.showMessage');
      try {
        const task = client.createTask('tasks.insertMessage');
        const { taskId } = task.applyAsync([null, user.username, user.room, msg]);
      } catch (error) {
        console.log(error);
      }
      io.to(user.room).emit('message', formatMessage(user.username, msg));
    });

    // Runs when client disconnects
    socket.on('disconnect', () => {
      const user = userLeave(socket.id);

      if (user) {
        io.to(user.room).emit(
          'message',
          formatMessage(botName, `${user.username} has left the chat`)
        );

        // Send users and room info
        io.to(user.room).emit('roomUsers', {
          room: user.room,
          users: getRoomUsers(user.room)
        });
      }
    });
  });
}

module.exports = getSocketRoutes