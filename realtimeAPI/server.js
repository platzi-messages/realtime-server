const path = require('path');
const http = require('http');
const express = require('express');
const getSocketRoutes = require('./getSocketRoutes');

const socketIo = require('socket.io')
const cors = require('cors')
const router = require('./router')

const app = express();
const server = http.createServer(app);
const io = socketIo(server)

app.use(cors())
app.use(router)

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

const PORT = process.env.PORT || 4007;
server.listen(PORT, () => console.log(`Server running on port ${PORT}`));

getSocketRoutes(io)

